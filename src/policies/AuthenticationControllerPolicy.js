const Joi = require('joi')

module.exports = {
    register(req, res, next) {
        const schema = {
            screenName: Joi.string().regex(
                new RegExp('^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$')
            ),
            email: Joi.string().email(),
            password: Joi.string().regex(
                new RegExp('^[a-zA-Z0-9]{8,32}$')
            )
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'email':
                    res.status(400).send({
                        error: 'You must supply a valid email address'
                    })
                    break

                case 'password':
                    res.status(400).send({
                        error: 'You must supply a valid password'
                    })
                    break

                case 'screenName':
                    res.status(400).send({
                        code: 'screen_name_missing',
                        error: 'You must supply a screen name'
                    })
                    break

                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    login(req, res, next) {
        const schema = {
            email: Joi.string().email(),
            password: Joi.string().regex(
                new RegExp('^[a-zA-Z0-9]{8,32}$')
            )
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {

            switch (error.details[0].context.key) {
                case 'email':
                    res.status(400).send({
                        error: 'You must supply a valid email address'
                    })
                    break

                case 'password':
                    res.status(400).send({
                        error: 'You must supply a valid password'
                    })
                    break

                default:
                    res.status(400).send({
                        error: 'Invalid login information'
                    })
            }
        } else {
            next()
        }
    },
    resetPlayerPassword(req, res, next) {

        const schema = {
            'email': Joi.string().email().required()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'email':
                    res.status(400).send({
                        error: 'You must supply a valid email address'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    }
}