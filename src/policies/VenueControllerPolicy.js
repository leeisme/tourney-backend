const Joi = require('joi')

module.exports = {
    create (req, res, next) {
        const schema = {
            name: Joi.string().regex(
                new RegExp('^[a-z|A-Z| ]+[a-z|A-Z|0-9 ]*$')
            ),
            active: Joi.boolean().truthy().required(),
            description: Joi.string().max(1024).trim(),
            phone: Joi.string().trim(),
            email: Joi.string().email(),
            web: Joi.string().uri()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'name':
                    res.status(400).send({
                        error: 'Invalid name. Must be a-z, A-Z, 0-9 starting with a Letter'
                    })
                    break
                case 'active':
                    res.status(400).send({
                        error: 'Boolean required. Can be truthy: true, 1, on'
                    })
                    break
                case 'description':
                    res.status(400).send({
                        error: 'Description must be a valid string'
                    })
                    break
                case 'phone':
                    res.status(400).send({
                        error: 'Phone must be a valid string repesentating a phone'
                    })
                    break
                case 'email':
                    res.status(400).send({
                        error: 'Email must be a valid email string'
                    })
                    break
                case 'web':
                    res.status(400).send({
                        error: 'Web must be a valid website url'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    update (req, res, next) {
        const schema = {
            id: Joi.number().integer().required().min(1),
            name: Joi.string().required().regex(
                new RegExp('^[a-z|A-Z| ]+[a-z|A-Z|0-9 ]*$')
            ),
            active: Joi.boolean().truthy().required(),
            description: Joi.string().max(1024).trim(),
            phone: Joi.string().trim(),
            email: Joi.string().email(),
            web: Joi.string().uri()
        }

        const combined = Object.assign(req.body, req.params)
        const {error, value} = Joi.validate(combined, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'name':
                    res.status(400).send({
                        error: 'Invalid name. Must be a-z, A-Z, 0-9 starting with a Letter'
                    })
                    break
                case 'active':
                    res.status(400).send({
                        error: 'Boolean required. Can be truthy: true, 1, on'
                    })
                    break
                case 'description':
                    res.status(400).send({
                        error: 'Description must be a valid string'
                    })
                    break
                case 'phone':
                    res.status(400).send({
                        error: 'Phone must be a valid string repesentating a phone'
                    })
                    break
                case 'email':
                    res.status(400).send({
                        error: 'Email must be a valid email string'
                    })
                    break
                case 'web':
                    res.status(400).send({
                        error: 'Web must be a valid website url'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    delete (req, res, next) {
        const schema = {
            id: Joi.number().integer().required()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'id':
                    res.status(400).send({
                        error: 'id is required | ' + error.details[0].message
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    }
}
