const Joi = require('joi')

module.exports = {
    idValidator(req, res, next) {
        const schema = {
            id: Joi.number().integer().min(1)
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'id':
                    res.status(400).send({
                        error: 'id is required'
                    })
                    break

                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    }
}