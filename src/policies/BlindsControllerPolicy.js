const Joi = require('joi')

module.exports = {
    create (req, res, next) {
        const schema = {
            name: Joi.string().regex(
                new RegExp('^[a-z|A-Z|]+[a-z|A-Z|0-9 ]*$')
            ),
            active: Joi.boolean().truthy()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'name':
                    res.status(400).send({
                        error: 'Invalid name. Must be a-z, A-Z, 0-9 starting with a Letter'
                    })
                    break
                case 'active':
                    res.status(400).send({
                        error: 'Must be true or false'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    update (req, res, next) {
        const schema = {
            id: Joi.number().integer().min(1),
            name: Joi.string().regex(
                new RegExp('^[a-z|A-Z|]+[a-z|A-Z|0-9 ]*$')
            ),
            active: Joi.boolean().truthy()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'id':
                    res.status(400).send({
                        error: 'Invalid id.'
                    })
                    break
                case 'name':
                    res.status(400).send({
                        error: 'Invalid name. Must be a-z, A-Z, 0-9 starting with a Letter'
                    })
                    break
                case 'active':
                    res.status(400).send({
                        error: 'Must be true or false'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid Blind Schedule information'
                    })
            }
        } else {
            next()
        }
    },
    createLevel (req, res, next) {

        const schema = {
            scheduleId: Joi.number().integer().min(1).required(),
            levelNumber: Joi.number().integer().min(1).required(),
            breakTime: Joi.number().integer().min(0).required(),
            smallBlind: Joi.number().integer().min(1).required(),
            bigBlind: Joi.number().integer().min(1).required(),
            ante: Joi.number().integer().min(0).required(),
            length: Joi.number().integer().min(1).required()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'scheduleId':
                    res.status(400).send({
                        error: 'scheduleId must be positive integer and point to valid Blind Schedule'
                    })
                    break
                case 'levelNumber':
                    res.status(400).send({
                        error: 'levelNumber must be integer number and minimum 1'
                    })
                    break
                case 'breakTime':
                    res.status(400).send({
                        error: 'breakTime must be integer number and minimum 0'
                    })
                    break
                case 'smallBlind':
                    res.status(400).send({
                        error: 'smallBlind must be integer number and minimum 1'
                    })
                    break
                case 'bigBlind':
                    res.status(400).send({
                        error: 'bigBlind must be integer number and minimum 1'
                    })
                    break
                case 'ante':
                    res.status(400).send({
                        error: 'ante must be integer number and minimum 0'
                    })
                    break
                case 'length':
                    res.status(400).send({
                        error: 'length must be integer number and minimum 0'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    updateLevel (req, res, next) {
        const schema = {
            id: Joi.number().integer().min(1).required(),
            scheduleId: Joi.number().integer().min(1).required(),
            levelNumber: Joi.number().integer().min(1).required(),
            breakTime: Joi.number().integer().min(0).required(),
            smallBlind: Joi.number().integer().min(1).required(),
            bigBlind: Joi.number().integer().min(1).required(),
            ante: Joi.number().integer().min(0).required(),
            length: Joi.number().integer().min(1).required()
        }

        const combined = Object.assign(req.body, req.params)
        const {error, value} = Joi.validate(combined, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'scheduleId':
                    res.status(400).send({
                        error: 'scheduleId must be positive integer and point to valid Blind Schedule'
                    })
                    break
                case 'levelNumber':
                    res.status(400).send({
                        error: 'levelNumber must be integer number and minimum 1'
                    })
                    break
                case 'breakTime':
                    res.status(400).send({
                        error: 'breakTime must be integer number and minimum 0'
                    })
                    break
                case 'smallBlind':
                    res.status(400).send({
                        error: 'smallBlind must be integer number and minimum 1'
                    })
                    break
                case 'bigBlind':
                    res.status(400).send({
                        error: 'bigBlind must be integer number and minimum 1'
                    })
                    break
                case 'ante':
                    res.status(400).send({
                        error: 'ante must be integer number and minimum 0'
                    })
                    break
                case 'length':
                    res.status(400).send({
                        error: 'length must be integer number and minimum 0'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid Blind Schedule information'
                    })
            }
        } else {
            next()
        }
    },
    changeLevelPosition (req, res, next) {
        const schema = {
            id: Joi.number().integer().required().min(1),
            scheduleId: Joi.number().integer().required().min(1),
            direction: Joi.string().valid(['increase', 'decrease'])
        }

        const combined = Object.assign(req.body, req.params)
        const {error, value} = Joi.validate(combined, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'id':
                    res.status(400).send({
                        error: 'id is required'
                    })
                    break
                case 'scheduleId':
                    res.status(400).send({
                        error: 'scheduleId pointing to valid BlindSchedule required'
                    })
                    break
                case 'direction':
                    res.status(400).send({
                        error: 'Must be string of either increase or decrease'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    deleteAllLevels (req, res, next) {
        const schema = {
            scheduleId: Joi.number().integer().required().min(1)
        }

        const combined = Object.assign(req.body, req.params)
        const {error, value} = Joi.validate(combined, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'scheduleId':
                    res.status(400).send({
                        error: 'scheduleId pointing to valid BlindSchedule required'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    }

}