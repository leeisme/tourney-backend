const Joi = require('joi')

module.exports = {
    create (req, res, next) {
        const schema = {
            name: Joi.string().regex(
                new RegExp('^[a-z|A-Z| ]+[a-z|A-Z|0-9 ]*$')
            ),
            startTime: Joi.date().required(),
            endTime: Joi.date().required()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'name':
                    res.status(400).send({
                        error: 'Invalid name. Must be a-z, A-Z, 0-9 starting with a Letter'
                    })
                    break
                case 'startTime':
                    res.status(400).send({
                        error: 'startTime is required and must be a valid date'
                    })
                    break
                case 'endTime':
                    res.status(400).send({
                        error: 'endTime is required and must be valid date later than startTime'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    update (req, res, next) {
        const schema = {
            id: Joi.number().integer().required().min(1),
            leagueId: Joi.number().integer().required().min(1),
            name: Joi.string().regex(
                new RegExp('^[a-z|A-Z| ]+[a-z|A-Z|0-9 ]*$')
            ),
            startTime: Joi.date().required(),
            endTime: Joi.date().required()
        }

        const combined = Object.assign(req.body, req.params)
        const {error, value} = Joi.validate(combined, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'id':
                    res.status(400).send({
                        error: 'id is required | ' + error.details[0].message
                    })
                    break
                case 'name':
                    res.status(400).send({
                        error: 'Invalid name. Must be a-z, A-Z, 0-9 starting with a Letter'
                    })
                    break
                case 'startTime':
                    res.status(400).send({
                        error: 'startTime is required and must be a valid date'
                    })
                    break
                case 'endTime':
                    res.status(400).send({
                        error: 'endTime is required and must be valid date later than startTime'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    delete (req, res, next) {
        const schema = {
            id: Joi.number().integer().required()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'id':
                    res.status(400).send({
                        error: 'id is required | ' + error.details[0].message
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    getList(req, res, next) {
        const schema = {
            pageNumber: Joi.number().integer().required().min(0),
            pageSize: Joi.number().integer().required().min(1).max(50)
        }

        const {error, value} = Joi.validate(req.params, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'pageNumber':
                    res.status(400).send({
                        error: 'pageNumber must be an integer minimum of 1'
                    })
                    break
                case 'pageSize':
                    res.status(400).send({
                        error: 'pageSize must be an integer minimum of 1, maximum of 50'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid request for Series list'
                    })
            }
        } else {
            next()
        }
    }
}
