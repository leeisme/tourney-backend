const Joi = require('joi')

module.exports = {
    create(req, res, next) {
        const schema = {
            name: Joi.string().regex(
                new RegExp('^[a-z|A-Z|]+[a-z|A-Z|0-9 ]*$')
            ),
            startTime: Joi.date().required(),
            seriesId: Joi.number().integer().required().min(1),
            leagueId: Joi.number().integer().required().min(1)
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'email':
                    res.status(400).send({
                        error: 'Invalid name. Must be a-z, A-Z, 0-9 starting with a Letter'
                    })
                    break
                case 'startTime':
                    res.status(400).send({
                        error: 'Must be a valid datetime'
                    })
                    break
                case 'seriesId':
                    res.status(400).send({
                        error: 'seriesId is required and must point to a valid Series'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid Tournament Data Supplied'
                    })
            }
        } else {
            next()
        }
    },
    update(req, res, next) {
        const schema = {
            id: Joi.number().integer().required().min(1),
            seriesId: Joi.number().integer().required().min(1),
            leagueId: Joi.number().integer().required().min(1),
            name: Joi.string().regex(
                new RegExp('^[a-z|A-Z|]+[a-z|A-Z|0-9 ]*$')
            ),
            startTime: Joi.date().required()
        }

        const combined = Object.assign(req.body, req.params)
        const {error, value} = Joi.validate(combined, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'id':
                    res.status(400).send({
                        error: 'id is required'
                    })
                    break
                case 'email':
                    res.status(400).send({
                        error: 'Invalid name. Must be a-z, A-Z, 0-9 starting with a Letter'
                    })
                    break

                case 'startTime':
                    res.status(400).send({
                        error: 'Must be a valid datetime'
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    },
    delete(req, res, next) {
        const schema = {
            id: Joi.number().integer().required()
        }

        const {error, value} = Joi.validate(req.body, schema)

        if (error) {
            switch (error.details[0].context.key) {
                case 'id':
                    res.status(400).send({
                        error: 'id is required | ' + error.details[0].message
                    })
                    break
                default:
                    res.status(400).send({
                        error: 'Invalid registration information'
                    })
            }
        } else {
            next()
        }
    }
}