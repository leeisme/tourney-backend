exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('blind_schedules').del()
    .then(function () {
        // Inserts seed entries
        return knex('blind_schedules').insert([
            {
                name: 'Test Blind Schedule',
                active: true,
                runTime: 60,
                leagueId: 1
            }
        ])
    })
}