const moment = require('moment')

exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('tournaments').del()
        .then(function () {
            // Inserts seed entries
            return knex('tournaments').insert([
                {
                    name: 'Test Tournament',
                    seriesId: 1,
                    leagueId: 1,
                    startTime: moment().toDate(),
                    runTime: 60 * 4
                }
            ])
        })
}