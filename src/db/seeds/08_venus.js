exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('venues').del()
    .then(function () {
        // Inserts seed entries
        return knex('venues').insert([
            {
                playerId: 1,
                name: 'Joe\'s bar and grill',
                active: true,
                description: 'Fun in the sun place to be',
                phone: '4103323422',
                email: 'you@you.com',
                web: 'you.com'
            }
        ])
    })
}