exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('blind_levels').del()
    .then(function () {
        // Inserts seed entries
        return knex('blind_levels').insert([
            {
                scheduleId: 1,
                levelNumber: 1,
                breakTime: 0,
                smallBlind: 10,
                bigBlind: 20,
                length: 20
            },
            {
                scheduleId: 1,
                levelNumber: 2,
                breakTime: 0,
                smallBlind: 20,
                bigBlind: 30,
                length: 20
            },
            {
                scheduleId: 1,
                levelNumber: 3,
                breakTime: 30,
                smallBlind: 30,
                bigBlind: 40,
                length: 20
            },
            {
                scheduleId: 1,
                levelNumber: 4,
                breakTime: 0,
                smallBlind: 10,
                bigBlind: 20,
                length: 20
            }
        ])
    })
}