exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('leagues').del()
        .then(function () {
            // Inserts seed entries
            return knex('leagues').insert([
                {
                    name: 'Test League',
                    active: true,
                    ownerId: 1
                }
            ])
        })
}