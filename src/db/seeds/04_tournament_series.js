const moment = require('moment')

exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('series').del()
    .then(function () {
        // Inserts seed entries
        return knex('series').insert([
            {
                name: 'Test Tournament Series.js',
                startTime: moment().add(1, 'days').toDate(),
                endTime: moment().add(90, 'days').toDate(),
                leagueId: 1
            }
        ])
    })
}