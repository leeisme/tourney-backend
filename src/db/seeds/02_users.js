const bcrypt = require('bcrypt-nodejs')

exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('users').del()

        .then(function () {
            // Inserts seed entries
            return knex('users').insert([
                {
                    email: 'admin@nodomain.com',
                    password: bcrypt.hashSync('12345678')
                }
            ])
        })
}
