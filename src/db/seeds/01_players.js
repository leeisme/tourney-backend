const faker = require('faker')
const bcrypt = require('bcrypt-nodejs')

exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('players').del()
        .then(function () {
            // Inserts seed entries
            return knex('players').insert([
                {
                    email: 'testplayer@nodomain.com',
                    password: bcrypt.hashSync('12345678'),
                    screenName: 'aces_up',
                    avatar: 'https://randomuser.me/api/portraits/men/85.jpg'
                }
            ])
        })
}
