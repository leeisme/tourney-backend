exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTable('players', table => {
            table.increments('id').primary()
            table.string('email')
            table.string('screenName')
            table.string('password')
            table.string('token', 2048)
            table.string('avatar', 1024).defaultTo(null)
            table.timestamps(true, true)
        })
    ])
}

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('players')
    ])
}
