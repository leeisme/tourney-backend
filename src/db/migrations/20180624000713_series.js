exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('series', table => {
            table.increments('id').primary()
            table.string('name').notNullable()
            table.dateTime('startTime').notNullable()
            table.dateTime('endTime').notNullable()
            table.integer('leagueId').unsigned().notNullable()
            table.foreign('leagueId').references('leagues.id')
            table.timestamps(true, true)
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('series')
    ])
};