exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('tournaments', table => {
            table.increments('id').primary()
            table.string('name').notNullable()
            table.dateTime('startTime').notNullable()
            table.integer('runTime').notNullable().defaultTo(0)
            table.timestamps(true, true)
            // FKs
            table.integer('seriesId').unsigned().notNullable()
            table.foreign('seriesId').references('series.id').onDelete('CASCADE')
            table.integer('leagueId').unsigned().notNullable()
            table.foreign('leagueId').references('leagues.id').onDelete('CASCADE')
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('tournaments')
    ])
};