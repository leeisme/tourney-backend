exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('blind_levels', table => {
            table.increments('id').primary()
            table.integer('levelNumber').notNullable()
            table.boolean('breakTime').notNullable().defaultsTo(0)
            table.decimal('smallBlind', 10, 2).notNullable().defaultsTo(0)
            table.decimal('bigBlind', 10, 2).notNullable().defaultTo(0)
            table.decimal('ante', 10, 2).notNullable().defaultTo(0)
            table.integer('length').notNullable()
            table.timestamps(true, true)
            // FKs
            table.integer('scheduleId').unsigned().notNullable()
            table.foreign('scheduleId').references('blind_schedules.id')
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('blind_levels')
    ])
};