exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('leagues_players', table => {
            table.increments('id').primary()
            table.integer('leagueId').notNullable().unsigned()
            table.integer('playerId').notNullable().unsigned()
            table.timestamps(true, true)
            table.index(['leagueId', 'playerId'])
            table.index(['playerId', 'leagueId'])
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('leagues_players')
    ])
};