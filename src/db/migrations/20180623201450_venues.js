exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTable('venues', table => {
            table.increments('id').primary()
            table.string('name')
            table.boolean('active').defaultTo(false)
            table.string('description', 1024)
            table.string('phone').nullable()
            table.string('email').nullable()
            table.string('web').nullable()
            table.timestamps(true, true)
            table.integer('playerId').unsigned().notNullable()
            table.foreign('playerId').references('players.id').onDelete('CASCADE')
        })
    ])
}

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('venues')
    ])
}
