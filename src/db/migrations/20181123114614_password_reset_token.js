exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('password_reset_tokens', table => {
            table.increments('id').primary()
            table.string('token', 2048)
            table.string('resetType').notNullable()
            table.dateTime('expiry').notNullable()
            table.integer('ownerId').unsigned().notNullable()
            table.timestamps(true, true)
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('password_reset_tokens')
    ])
};