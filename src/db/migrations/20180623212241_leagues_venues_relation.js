exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('leagues_venues', table => {
            table.increments('id').primary()
            table.integer('leagueId').notNullable()
            table.integer('venueId').notNullable()
            table.timestamps(true, true)
            table.index(['venueId', 'leagueId'])
            table.index(['leagueId', 'venueId'])
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('leagues_venues')
    ])
};