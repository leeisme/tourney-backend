exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('leagues', table => {
            table.increments('id').primary()
            table.string('name').notNullable()
            table.boolean('active').defaultTo(false)
            table.string('phone').nullable()
            table.string('email').nullable()
            table.string('web').nullable()
            // FKs
            table.integer('ownerId').unsigned().notNullable()
            table.foreign('ownerId').references('players.id')
            table.timestamps(true, true)
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('leagues')
    ])
};