exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('series_tournament', table => {
            table.increments('id').primary()
            table.integer('seriesId').notNullable()
            table.integer('tournamentId').notNullable()
            table.timestamps(true, true)
            table.index(['seriesId', 'tournamentId'])
            table.index(['tournamentId', 'seriesId'])
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('series_tournament')
    ])
};