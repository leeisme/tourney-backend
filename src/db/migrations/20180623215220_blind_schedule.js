exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('blind_schedules', table => {
            table.increments('id').primary()
            table.string('name')
            table.boolean('active').defaultTo(false)
            table.integer('runTime').notNullable().defaultTo(0)
            table.timestamps(true, true)
            // FKs
            table.integer('leagueId').unsigned().notNullable()
            table.foreign('leagueId').references('leagues.id')
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('blind_schedules')
    ])
};