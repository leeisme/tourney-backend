const Jwt = require('jsonwebtoken')
const config = require('../config/config')

module.exports = {
    signUser (player) {

        return new Promise((resolve, reject) => {
            const ONE_WEEK = 60 * 60 * 24 * 7

            return Jwt.sign(player, config.authentication.jwSecret, {
                expiresIn: '1 day'
            }, (err, res) => {

                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    },
    verify (token) {
        return new Promise((resolve, reject) => {
            return Jwt.verify(token, config.authentication.jwSecret,
                (err, res) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(res)
                    }
                })
        })
    }
}