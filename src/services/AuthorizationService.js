const Player = require('../models/Player')
const PasswordResetToken = require('../models/PasswordResetToken')
const crypto = require('crypto')
const moment = require('moment')

module.exports = {
    async screenNameInUse(username) {
        const player = await Player.query()
            .findOne({screenName: username})

        return player !== undefined
    },
    /**
     * Resets a player record and requests a new password. Assigns temporary
     * password and creates password reset token for the user.
     * @param email string
     * @return {Promise<*>}
     */
    async resetPlayerPassword(email) {
        // Validate the player

        let player = await Player.query()
            .findOne({email: email});

        if (!player) {
            return null
        }

        // Generate token, random password and assign them
        const token =  crypto.randomBytes(
            Number(process.env.PASSWORD_RESET_TOKEN_SIZE)
        ).toString('base64')

        const password =  crypto.randomBytes(
            Number(process.env.PASSWORD_RESET_SIZE)
        ).toString('base64')

        // Insert the token
        const expiry = moment().add(30, 'days').toDate()

        result = await PasswordResetToken
            .query()
            .insert({
                token: token,
                resetType: 'player',
                ownerId: player.id,
                expiry: expiry
            })

        // Clean up the objects and return a response
        await Player.query()
            .patch({
                token: token,
                password: password
            })
            .where('email', '=', email)

        return {
            token: token,
            expires: expiry.toDateString()
        }
    }
}
