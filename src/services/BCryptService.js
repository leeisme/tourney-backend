const bcrypt = require('bcrypt-nodejs')

module.exports = {
    // first generate a random salt
    genSalt (value, workforce) {
        if (!workforce) {
            workforce = 8
        }

        return new Promise((resolve,reject) => {
            bcrypt.genSalt(workforce, function(err,salt) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve({
                        salt:salt
                    });
                }
            })
        })
    },
    genHash (value, salt) {

        return new Promise((resolve,reject) => {
            bcrypt.hash(value,salt,null,function(err,hash) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve({
                        salt:salt,
                        value: value,
                        hash:hash
                    });
                }
            })
        })
    },
    compareHash (value, hash) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(value, hash, function(err, res) {

                if (err) {
                    reject(err)
                } else {
                    resolve(res)
                }
            })
        })
    }
}