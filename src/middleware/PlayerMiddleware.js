module.exports = {
    async verifyPlayer(req, res, next) {
        const token = req.body.token || req.query.token || req.headers['x-access-token']

        if (!token) {
            return res.status(403).send({
                error: 'Invalid or missing token'
            });
        }

        try {
            if (!req.decoded.playerId) {
                return res.status(403).send({
                    error: 'Not a player'
                })
            }
        } catch(err) {
            let msg = null;

            if (err.expiredAt) {
                msg = 'Invalid token: expired'
            } else {
                msg = 'Invalid token'
            }

            return res.status(403).send({
                error: msg
            })
        }

        next()
    }
}
