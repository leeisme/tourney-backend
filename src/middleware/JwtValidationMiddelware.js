const JwtService = require('../services/JwtService')
const config = require('../config/config')

module.exports = {
    async verifyToken(req, res, next) {
        const token = req.body.token || req.query.token || req.headers['x-access-token']

        if (!token) {
            return res.status(403).send({
                error: 'Invalid or missing token'
            });
        }

        try {
            req.decoded = await JwtService.verify(token, config.authentication.jwSecret)
        } catch(err) {
            let msg = null;

            if (err.expiredAt) {
                msg = 'Invalid token: expired'
            } else {
                msg = 'Invalid token'
            }

            return res.status(403).send({
                error: msg
            })
        }

        next()
    }
}
