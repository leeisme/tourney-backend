const router = require('express').Router()
const CommonControllerPolicy = require('../policies/CommonControllerPolicy')
const JwtValidationMiddleware = require('../middleware/JwtValidationMiddelware')
const TournamentControllerPolicy = require('../policies/TournamentControllerPolicy')
const TournamentController = require('../controllers/TournamentController')
const SeriesControllerPolicy = require('../policies/SeriesControllerPolicy')
const SeriesController = require('../controllers/SeriesController')
const BlindsControllerPolicy = require('../policies/BlindsControllerPolicy')
const BlindsController = require('../controllers/BlindController')

// Series

router.post('/series',
    JwtValidationMiddleware.verifyToken,
    SeriesControllerPolicy.create,
    SeriesController.create
)

router.get('/series/:pageNumber/:pageSize',
    JwtValidationMiddleware.verifyToken,
    SeriesControllerPolicy.getList,
    SeriesController.getList
)

router.patch('/series/:id',
    JwtValidationMiddleware.verifyToken,
    SeriesControllerPolicy.update,
    SeriesController.update
)

router.delete('/series/:id',
    JwtValidationMiddleware.verifyToken,
    SeriesController.delete
)

// Tournament

router.post('/tournament',
    JwtValidationMiddleware.verifyToken,
    TournamentControllerPolicy.create,
    TournamentController.create
)

router.patch('/tournament/:id',
    JwtValidationMiddleware.verifyToken,
    TournamentControllerPolicy.update,
    TournamentController.update
)

router.delete('/tournament/:id',
    JwtValidationMiddleware.verifyToken,
    CommonControllerPolicy.idValidator,
    TournamentController.delete
)

// Blind Schedule

router.post('/blind-schedule',
    JwtValidationMiddleware.verifyToken,
    BlindsControllerPolicy.create,
    BlindsController.createSchedule
)

router.patch('/blind-schedule/:id',
    JwtValidationMiddleware.verifyToken,
    BlindsControllerPolicy.update,
    BlindsController.updateBlindSchedule
)

router.delete('/blind-schedule/:id',
    JwtValidationMiddleware.verifyToken,
    CommonControllerPolicy.idValidator,
    BlindsController.deleteBlindSchedule
)

router.post('/blind-level',
    JwtValidationMiddleware.verifyToken,
    BlindsControllerPolicy.createLevel,
    BlindsController.createLevel
)

router.patch('/blind-level/:id',
    JwtValidationMiddleware.verifyToken,
    BlindsControllerPolicy.updateLevel,
    BlindsController.updateLevel
)

router.patch('/blind-level/position/:id',
    JwtValidationMiddleware.verifyToken,
    BlindsControllerPolicy.changeLevelPosition,
    BlindsController.changeLevelPosition
)

router.delete('/blind-level/delete-all/:scheduleId',
    JwtValidationMiddleware.verifyToken,
    BlindsControllerPolicy.deleteAllLevels,
    BlindsController.deleteAllLevels
)

module.exports = router
