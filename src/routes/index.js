
module.exports = function(app) {
    app.use('/api', require('./authentication'))
    app.use('/api', require('./tournament'))
    app.use('/api', require('./venue'))
}