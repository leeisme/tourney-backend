const router = require('express').Router()
const CommonControllerPolicy = require('../policies/CommonControllerPolicy')
const JwtValidationMiddleware = require('../middleware/JwtValidationMiddelware')
const PlayerMiddleware = require('../middleware/PlayerMiddleware')
const VenueControllerPolicy = require('../policies/VenueControllerPolicy')
const VenueController = require('../controllers/VenueController')

// Series

router.post('/venue',
    JwtValidationMiddleware.verifyToken,
    PlayerMiddleware.verifyPlayer,
    VenueControllerPolicy.create,
    VenueController.create
)

router.patch('/venue/:id',
    JwtValidationMiddleware.verifyToken,
    PlayerMiddleware.verifyPlayer,
    VenueControllerPolicy.update,
    VenueController.update
)

router.delete('/venue/:id',
    JwtValidationMiddleware.verifyToken,
    CommonControllerPolicy.idValidator,
    VenueController.delete
)

module.exports = router
