const router = require('express').Router()
const AuthenticationController = require('../controllers/AuthenticationController')
const AuthenticationControllerPolicy = require('../policies/AuthenticationControllerPolicy')

router.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register
)

router.post('/reset/player',
    AuthenticationControllerPolicy.resetPlayerPassword,
    AuthenticationController.playerPasswordReset
)

router.post('/login/player',
    AuthenticationControllerPolicy.login,
    AuthenticationController.playerLogin
)

router.post('/login/user',
    AuthenticationControllerPolicy.login,
    AuthenticationController.userLogin
)

module.exports = router
