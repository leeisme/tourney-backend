const router = require('express').Router()
const BlindController = require('../controllers/BlindController')
const JwtVaidatoinMiddleware = require('../middleware/JwtValidationMiddelware')

// const BlindControllerPolicy = require('../policies/BlindsControllerPolicy')
// const CommonControllerPolicy = require('../policies/CommonControllerPolicy')

router.post('/blind-schedule/create',
    JwtVaidatoinMiddleware.verifyToken,
    BlindController.createSchedule
)

// router.post('/api/blind-schedule/update',
//     CommonControllerPolicy.idValidator,
//     BlindControllerPolicy.blindSchedule,
//     BlindController.updateBlindSchedule
// )
//
// router.post('/blind-schedule/delete',
//     CommonControllerPolicy.idValidator,
//     BlindController.deleteBlindSchedule
// )

module.exports = router
