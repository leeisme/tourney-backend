const {Model} = require('objection')

class BlindLevel extends Model {
    static get tableName () {
        return 'blind_levels'
    }

    static get relationMappings () {
        const BlindSchedule = require('./BlindSchedule')

        return {
            schedule: {
                relation: Model.BelongsToOneRelation,
                modelClass: BlindSchedule,
                join: {
                    from: 'blind_levels.scheduleId',
                    to: 'blind_schedules.id'
                }
            }
        }

    }
}

module.exports = BlindLevel
