const { Model } = require('objection')
const Player = require('../models/Player')
const League = require('../models/League')

class Venue extends Model {
    static get tableName () {
        return 'venues'
    }

    static get relationMappings() {
        return {
            leagues: {
                relation: Model.ManyToManyRelation,
                modelClass: League,
                join: {
                    from: 'venues.id',
                    through: {
                        from: 'leagues_venues.venueId',
                        to: 'leagues_venues.leagueId'
                    },
                    to: 'leagues.id'
                }
            },
            player: {
                relation: Model.BelongsToOneRelation,
                modelClass: Player,
                join: {
                    from: 'venues.playerId',
                    to: 'players.id'
                }
            }
        }
    }
}

module.exports = Venue
