const {Model} = require('objection')

class League extends Model {
    static get tableName () {
        return 'leagues'
    }

    static get relationMappings () {
        const Player = require('./Player')

        return {
            owner: {
                relation: Model.BelongsToOneRelation,
                modelClass: Player,
                join: {
                    from: 'leagues.id',
                    to: 'players.id'
                }
            },
            members: {
                relation: Model.ManyToManyRelation,
                modelClass: Player,
                join: {
                    from: 'leagues.id',
                    through: {
                        from: 'leagues_players.leagueId',
                        to: 'leagues_players.playerId'
                    },
                    to: 'players.id'
                }
            }
        }
    }
}

module.exports = League
