const {Model} = require('objection')

class Tournament extends Model {
    static get tableName () {
        return 'tournaments'
    }

    static get relationMappings () {
        const Series = require('./Series')
        const League = require('./League')

        return {
            schedule: {
                relation: Model.BelongsToOneRelation,
                modelClass: Series,
                join: {
                    from: 'tournaments.seriesId',
                    to: 'series.id'
                }
            },
            league: {
                relation: Model.BelongsToOneRelation,
                modelClass: League,
                join: {
                    from: 'tournaments.leagueId',
                    to: 'leagues.id'
                }
            }
        }
    }
}

module.exports = Tournament
