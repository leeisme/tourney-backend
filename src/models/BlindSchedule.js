const {Model} = require('objection')

class BlindSchedule extends Model {
    static get tableName () {
        return 'blind_schedules'
    }

    static get relationMappings () {
        const BlindLevel = require('./BlindLevel')
        return {
            levels: {
                relation: Model.HasManyRelation,
                modelClass: BlindLevel,
                join: {
                    from: 'blind_schedules.id',
                    to: 'blind_levels.scheduleId'
                }
            }
        }

    }
}

module.exports = BlindSchedule
