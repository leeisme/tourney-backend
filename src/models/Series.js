const { Model } = require('objection')

class Series extends Model {
    static get tableName () {
        return 'series'
    }

    static get relationMappings() {
        const League = require('./League')
        return {
            league: {
                relation: Model.BelongsToOneRelation,
                modelClass: League,
                join: {
                    from: 'series.leagueId',
                    to: 'leagues.id'
                }
            }
        }
    }
}

module.exports = Series
