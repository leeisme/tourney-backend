const {Model} = require('objection')
const bcryptService = require('../services/BCryptService')

class Player extends Model {
    static get tableName () {
        return 'players'
    }

    async hashPassword (user) {

        await bcryptService.genSalt(user.password)
            .then((result) => {
                bcryptService.genHash(user.password, result.salt)
                    .then((result) => {
                        this.password = result.hash
                    })
            })
    }

    static get relationMappings() {

        const League = require('./League')

        return {
            leagueMemberships: {
                relation: Model.ManyToManyRelation,
                modelClass: League,
                from: 'players.id',
                join: {
                    from: 'players.id',
                    through: {
                        from: 'leagues_players.playerId',
                        to: 'leagues_players.leagueId'
                    },
                    to: 'leagues.id'
                }
            },
            league: {
                relation: Model.HasOneRelation,
                modelClass: League,
                join: {
                    from: 'players.id',
                    to: 'leagues.ownerId'
                }
            }
        }
    }

    async $beforeInsert (queryContext) {
        await super.$beforeInsert(queryContext)
        this.hashPassword(this)
    }
}

module.exports = Player
