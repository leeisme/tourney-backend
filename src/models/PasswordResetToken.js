const { Model } = require('objection')

class PasswordResetToken extends Model {
    static get tableName () {
        return 'password_reset_tokens'
    }
}

module.exports = PasswordResetToken
