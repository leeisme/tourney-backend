module.exports = {
    port: 3000,
    db: {
        database: process.env.DB_NAME || 'tourney',
        user: process.env.DB_USER || 'root',
        password: process.env.DB_PASS || 'root',
        options: {
            dialect: process.env.DIALECT || 'mysql',
            host: process.env.DB_HOST || 'tourney.test',
            port: process.env.DB_PORT || 3306
        }
    },
    authentication: {
        jwSecret: process.env.JWT_SECRET || 'secret'
    }
}
