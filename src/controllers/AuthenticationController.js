const Player = require('../models/Player')
const User = require('../models/User')
const Jwt = require('jsonwebtoken')
const config = require('../config/config')
const bcryptService = require('../services/BCryptService')
const AuthService = require('../services/AuthorizationService')
const JwtService = require('../services/JwtService')

/**
 * Signs a user object with a jwt token and returns the result.
 * @param player
 * @return {*}
 */
function jwtSignUser (player) {
    const ONE_WEEK = 60 * 60 * 24 * 7
    return Jwt.sign(player, config.authentication.jwSecret, {
        expiresIn: ONE_WEEK
    })
}

/**
 * Public representation of a player, suitable for public consumption
 * @param player
 * @return {{id: *, email: *, screenName: (string), avatar: (string|*)}}
 */
function makePublicPlayer (player) {
    return {
        id: player.id,
        email: player.email,
        screenName: player.screenName,
        avatar: player.avatar
    }
}

module.exports = {
    /**
     * Register a user with the system. Basic player registration which gives access
     * to join leagues or create a league(s) of their own.
     * @param req
     * @param res
     * @return {Promise<void>}
     */
    async register (req, res) {
        try {

            // Check username
            if (await AuthService.screenNameInUse(req.body.screenName) !== false) {
                res.status(400).send({
                    code: 'screen_name_taken',
                    error: 'Screen Name is already in use'
                })
            } else {
                const player = await Player.query().insertGraph(req.body)
                res.send(makePublicPlayer(player))
            }

        } catch (err) {
            console.log(err)
            res.status(400).send({
                error: error.message
            })
        }
    },
    /**
     * Login a user as a player.
     * @param req
     * @param res
     * @return {Promise<*>}
     */
    async playerLogin (req, res) {
        try {
            const {email, password} = req.body

            let player = await Player
                .query()
                .eager('league')
                .findOne({email: email})


            if (!player) {
                return res.status(403).send({
                    error: 'The login information was not correct'
                })
            }

            isPasswordValid = await bcryptService.compareHash(
                password,
                player.password
            )

            if (!isPasswordValid) {
                res.status(403).send({
                    error: 'The login information was incorrect'
                })
            }

            // If token is set, remove it and issue another
            if (player.token) {
                player.token = null
            }

            const payload = {
                user: makePublicPlayer(player),
                leagueId: player.league.id,
                iat: Math.floor(Date.now() / 1000) - 3000
            }

            if (player.league) {
                payload.playerId = player.id
            }

            const token = await JwtService.signUser(payload)

            player = await Player.query()
                .patch({token: token})
                .where('id', '=', player.id)

            payload.token = token

            delete(payload.leagueId)
            delete(payload.playerId)
            res.send(payload)
        } catch (err) {
            console.log(err)
            res.status(500).send({
                error: 'An error has occurred while trying to login'
            })
        }
    },
    /**
     * Request a password reset for a player.
     * @param req
     * @param res
     * @return {Promise<*>}
     */
    async playerPasswordReset (req, res) {
        try {
            const {email} = req.body

            const result = await AuthService.resetPlayerPassword(email)

            if (null === result) {
                return res.status(500).send({
                    error: 'An error has occurred while resetting the password'
                })
            }

            res.send(result)

        } catch (err) {
            console.log(err)
            res.status(500).send({
                error: err.message
            })
        }
    },
    /**
     * Login a User of the system, typically someone with admin role
     * @param req
     * @param res
     * @return {Promise<*>}
     */
    async userLogin (req, res) {
        try {

            const {email, password} = req.body

            let user = await User
                .query()
                .findOne({email: email})

            if (!user) {
                return res.status(403).send({
                    error: 'The login information was not correct'
                })
            }

            isPasswordValid = await bcryptService.compareHash(
                password,
                user.password
            )

            if (!isPasswordValid) {
                return res.status(403).send({
                    error: 'The login information was incorrect'
                })
            }

            const token = jwtSignUser(user.toJSON())
            const payload = {
                token: token,
                user: makePublicPlayer(user),
                userId: user.id
            }

            user = await User.query()
                .patch({token: token})
                .where('id', '=', user.id)

            res.send(payload)
        } catch (err) {
            console.log(err)
            res.status(500).send({
                error: 'An error has occurred while trying to login'
            })
        }
    }
}
