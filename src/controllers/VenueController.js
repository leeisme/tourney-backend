const Venue = require('../models/Venue')

module.exports = {
    async create (req, res) {

        try {
            const venue = await Venue.query().insertGraph({
                playerId: req.decoded.playerId,
                name: req.body.name,
                active: true,
                description: req.body.description,
                phone: req.body.phone,
                email: req.body.email,
                web: req.body.web
            })

            res.json(venue)

        } catch (err) {
            console.log(err)
            res.status(400).send({
                error: error.message
            })
        }
    },
    async update (req, res) {
        try {
            await Venue.query()
                .patch({
                    playerId: req.decoded.playerId,
                    name: req.body.name,
                    active: true,
                    description: req.body.description,
                    phone: req.body.phone,
                    email: req.body.email,
                    web: req.body.web
                })
                .where('id', '=', req.params.id)
                .andWhere('playerId', '=', req.decoded.playerId)

            return res.json({
                success: true
            })

        } catch (err) {
            console.log(err.message)
            res.status(500).send({
                error: 'Server side error occurred attempting to fill request'
            })
        }
    },
    async delete (req, res) {
        try {
            const result = await Venue.query()
                .delete()
                .where('id', '=', req.params.id)

            res.send({success: true})
        } catch (err) {
            console.log(err)
            res.status(500).send({
                error: 'Server side error occurred attempting to fill request'
            })
        }
    }
}
