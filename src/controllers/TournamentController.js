const Tournament = require('../models/Tournament')
const moment = require('moment')

module.exports = {
    async create(req, res) {
        try {
            const tourney = await Tournament.query()
                .insertGraph({
                    name: req.body.name,
                    startTime: moment(req.body.startTime).toDate(),
                    seriesId: req.body.seriesId,
                    leagueId: req.decoded.leagueId
                })


            return res.json(tourney)

        } catch(err) {
            res.status(400).send({
                error: "Error attempt to create tournament"
            })
        }
    },
    async update(req, res) {
        try {
            const affected = await Tournament.query()
                .patch({
                    name: req.body.name,
                    startTime: moment(req.body.startTime).toDate(),
                    seriesId: req.body.seriesId,
                    leagueId: req.body.leagueId
                })
                .where('id', '=', req.params.id)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            return res.json({
                success: true,
                affected: affected
            })
        } catch (err) {
            console.log(err)
            res.status(400).send({
                error: "Error attempt to create tournament"
            })
        }
    },
    async delete(req, res) {
        try {
            const affected = await Tournament.query()
                .delete()
                .where('id', '=', req.params.id)

            res.send({success: true})
        } catch(err) {
            console.log(err)
            res.status(400).send({
                error: 'Error deleting the object specified'
            })
        }
    }
}