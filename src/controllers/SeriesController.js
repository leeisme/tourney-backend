const Series = require('../models/Series')
const moment = require('moment')

module.exports = {
    async create (req, res) {
        try {
            // Add the league
            req.body.leagueId = req.decoded.leagueId
            const series = await Series.query().insertGraph({
                name: req.body.name,
                startTime: moment(req.body.startDate).toDate(),
                endTime: moment(req.body.endDate).toDate(),
                leagueId: req.decoded.leagueId
            })

            res.send(series)
        } catch (err) {
            console.log(err)
            res.status(400).send({
                error: error.message
            })
        }
    },
    async update (req, res) {
        try {
            // Update the league
            req.body.leagueId = req.decoded.leagueId

            result = await Series.query()
                .patch({
                    name: req.body.name,
                    startTime: moment(req.body.startDate).toDate(),
                    endTime: moment(req.body.endDate).toDate(),
                    leagueId: req.decoded.leagueId
                })
                .where('id', '=', req.params.id)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            res.send(req.body)
        } catch (err) {
            res.status(500).send({
                error: 'An error has occurred while trying to update'
            })
        }
    },
    async delete (req, res) {
        try {
            await Series.query()
                .delete()
                .where('id', '=', req.params.id)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            res.send({success: true})
        } catch (err) {
            console.log(err)
            res.status(500).send({
                error: 'An error has occurred while trying to delete'
            })
        }
    },
    async getList(req, res) {
        try {
            const result = await Series.query()
                .where('leagueId', '=', req.decoded.leagueId)
                .page(req.params.pageNumber, req.params.pageSize)

            res.json(result)
        } catch(err) {
            console.log(err)
            res.status(500).send({
                error: 'An error has occurred while trying retrieve list'
            })
        }
    }
}
