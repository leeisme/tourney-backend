const BlindSchedule = require('../models/BlindSchedule')
const BlindLevel = require('../models/BlindLevel')
const queryBuilder =

module.exports = {
    async createSchedule(req, res) {

        try {
            req.body.leagueId = req.decoded.leagueId

            result = await BlindSchedule
                .query()
                .insert(req.body)

            return res.json(result)

        } catch(err) {
            res.status(400).send({
                error: "Email account already in use"
            })
        }
    },
    async updateBlindSchedule(req, res) {
        try {
            const affected = await BlindSchedule.query()
                .patch({
                    name: req.body.name,
                    active: req.body.active
                })
                .where('id', '=', req.params.id)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            return res.json({
                success: true,
                affected: affected
            })

        } catch(err) {
            res.status(500).send({
                error: 'Error occurred attempting to fulfill request'
            })
        }
    },
    async deleteBlindSchedule(req, res) {
        try {
            const result = await BlindSchedule.query()
                .delete()
                .where('id', '=', req.params.id)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            res.send({success: true})
        } catch (err) {
            console.log(err)
            res.status(500).send({
                error: 'An error has occurred while trying to login'
            })
        }
    },
    async createLevel(req, res) {
        try {
            // Validate that scheduleId points to valid, owned BlindSchedule
            const schedule = BlindSchedule.query()
                .where('id', '=', req.body.scheduleId)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            if (!schedule) {
                return res.status(404).send({
                    error: 'Valid Blind Schedule not found for BlindLevel'
                })
            }

            const result = await BlindLevel.query().insertGraph({
                scheduleId: req.body.scheduleId,
                levelNumber: req.body.levelNumber,
                breakTime: req.body.breakTime,
                smallBlind: req.body.smallBlind,
                bigBlind: req.body.bigBlind,
                ante: req.body.ante,
                length: req.body.length
            })

            return res.json(result)

        } catch(err) {
            console.log(err)
            res.status(400).send({
                error: "Problem creating BlindLevel"
            })
        }
    },
    async updateLevel(req, res) {
        try {
            // Validate that scheduleId points to valid, owned BlindSchedule
            const schedule = BlindSchedule.query()
                .where('id', '=', req.body.scheduleId)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            if (!schedule) {
                return res.status(404).send({
                    error: 'Valid Blind Schedule not found for BlindLevel'
                })
            }

            const affected = await BlindLevel.query()
                .patch({
                    scheduleId: req.body.scheduleId,
                    levelNumber: req.body.levelNumber,
                    breakTime: req.body.breakTime,
                    smallBlind: req.body.smallBlind,
                    bigBlind: req.body.bigBlind,
                    ante: req.body.ante,
                    length: req.body.length
                })
                .where('id', '=', req.params.id)

            return res.json({
                success: true,
                affected: affected
            })

        } catch(err) {
            res.status(500).send({
                error: 'Error occurred attempting to fulfill request'
            })
        }
    },
    async changeLevelPosition(req, res) {
        try {
            let otherLevels = null

            // Validate that scheduleId points to valid, owned BlindSchedule
            const schedules = await BlindSchedule.query()
                .where('id', '=', req.body.scheduleId)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            if (schedules.length === 0) {
                return res.status(404).send({
                    error: 'Valid Blind Schedule not found for BlindLevel'
                })
            }

            // Grab the BlindLevel in question
            let blindLevels = await BlindLevel.query()
                .where('id', '=', req.params.id)

            if (blindLevels.length === 0) {
                return res.status(404).send({
                    error: 'Blind Level not found'
                })
            }

            const level = blindLevels[0];

            if (req.body.direction === 'decrease') {
                if (level.levelNumber <= 1) {
                    return res.status(403).send({
                        error: 'Requested position not allowed'
                    })
                }

                // Get the blind level before current position
                otherLevels = await BlindLevel.query()
                    .where('levelNumber', '=', level.levelNumber - 1)
                    .andWhere('scheduleId', '=', req.body.scheduleId)

                if (otherLevels.length === 0) {
                    return res.status(403).send({
                        error: 'The BlindLevel is already the first level'
                    })
                }

            } else if (req.body.direction === 'increase') {

                // Get the blind level after current position
                otherLevels = await BlindLevel.query()
                    .where('levelNumber', '=', level.levelNumber + 1)
                    .andWhere('scheduleId', '=', req.body.scheduleId)

                if (otherLevels.length === 0) {
                    return res.status(403).send({
                        error: 'The current BlindLevel is the last one'
                    })
                }

            }

            const otherLev = otherLevels[0]

            // Switch them
            await BlindLevel.query()
                .patch({levelNumber: 500})
                .where('id', '=', otherLev.id)

            await BlindLevel.query()
                .patch({levelNumber: 1000})
                .where('id', '=', level.id)

            const result = [
                {id: level.id, levelNumber: otherLev.levelNumber},
                {id: otherLev.id, levelNumber: level.levelNumber}
            ]

            return res.json(result)

        } catch(err) {
            res.status(500).send({
                error: 'Error occurred attempting to fulfill request'
            })
        }
    },
    async deleteAllLevels(req, res) {
        try {

            // Validate that scheduleId points to valid, owned BlindSchedule
            const schedules = await BlindSchedule.query()
                .where('id', '=', req.params.scheduleId)
                .andWhere('leagueId', '=', req.decoded.leagueId)

            if (schedules.length === 0) {
                return res.status(404).send({
                    error: 'Valid Blind Schedule not found for BlindLevel'
                })
            }

            await BlindLevel.query()
                .delete()
                .where('scheduleId', '=', req.params.scheduleId)

            return res.json({
                success: true
            })

        } catch(err) {
            res.status(500).send({
                error: 'Error occurred attempting to fulfill request'
            })
        }
    }

}