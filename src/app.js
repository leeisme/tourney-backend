const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const config = require('./config/config')
const Knex = require('knex')
const knexConfig = require('../knexfile')
const { Model } = require('objection')

require('dotenv').config()

const app = express()
app.use(bodyParser.json())
app.use(cors())

// Setup logging when not in test mode
// if (process.env.NODE_ENV !== 'test') {
app.use(morgan('combined'))

// }

// Initialize knex.
const knex = Knex(knexConfig.development)

// Bind all Models to a knex instance. If you only have one database in
// your server this is all you have to do. For multi database systems, see
// the Model.bindKnex method.
Model.knex(knex)

require('./routes/index')(app)

app.listen(process.env.PORT || 3000)
console.log(`Server started on port ${config.port}`)

module.exports = app
