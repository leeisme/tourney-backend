/* global it, describe, beforeEach */
'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
chai.should()
const chaiHttp = require('chai-http')
const server = require('../src/app')
const TokenInserter = require('../test-utilty/TokenInserter')
const TestSeeder = require('../test-utilty/TestSeeder')
const BlindLevel = require('../src/models/BlindLevel')
chai.use(chaiHttp)

describe('CRUD Blind Schedule', () => {
    beforeEach(async () => {
        await BlindLevel.query()
            .delete()
            .where('id', '>', 1)

        const schedule = await TestSeeder.createBlindSchedule()
        this.scheduleId = schedule.id
        const level = await TestSeeder.createBlindLevel(1, 10, 20)
        const level2 = await TestSeeder.createBlindLevel(2, 100, 500)
        const level3 = await TestSeeder.createBlindLevel(3, 200, 400)
        this.levelId = level.id
        this.level2Id = level2.id
        this.level3Id = level3.id

        this.token = await TokenInserter.insertTokenForTestPlayer()
    })

    it('it should create a BlindSchedule', (done) => {
        const payload = {
            name: 'Test Blind Schedule',
            active: true
        }

        chai.request(server)
            .post('/api/blind-schedule')
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('id')
                    res.body.should.have.property('name')
                    res.body.should.have.property('active')
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should update a Blind Schedule', (done) => {
        const payload = {
            name: 'Updated Blind Schedule',
            active: true
        }

        chai.request(server)
            .patch('/api/blind-schedule/' + this.scheduleId)
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('success').equal(true)
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should delete a Blind Schedule', (done) => {
        chai.request(server)
            .delete('/api/blind-schedule/' + this.scheduleId)
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should create a blind level', (done) => {
        const payload = {
            scheduleId: 1,
            levelNumber: 1,
            breakTime: 0,
            smallBlind: 10,
            bigBlind: 20,
            ante: 0,
            length: 20
        }

        chai.request(server)
            .post('/api/blind-level')
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('id')
                    res.body.should.have.property('levelNumber')
                    res.body.should.have.property('breakTime')
                    res.body.should.have.property('bigBlind')
                    res.body.should.have.property('smallBlind')
                    res.body.should.have.property('ante')
                    res.body.should.have.property('length')
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should update a Blind Level', (done) => {
        const payload = {
            scheduleId: 1,
            levelNumber: 1,
            breakTime: 0,
            smallBlind: 100,
            bigBlind: 200,
            ante: 0,
            length: 20
        }

        chai.request(server)
            .patch('/api/blind-level/' + this.levelId)
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('success').equal(true)
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should delete a Blind Level', (done) => {
        chai.request(server)
            .delete('/api/blind-schedule/' + this.level3Id)
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should increase the levelNumber of a blind level', (done) => {
        const payload = {
            scheduleId: 1,
            direction: 'increase'
        }

        chai.request(server)
            .patch('/api/blind-level/position/' + this.levelId)
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('array').to.have.length(2)
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should decrease the levelNumber of a blind level', (done) => {
        const payload = {
            scheduleId: 1,
            direction: 'decrease'
        }

        chai.request(server)
            .patch('/api/blind-level/position/' + this.level3Id)
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('array').to.have.length(2)
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should delete all BlindLevels for a BlindSchedule', (done) => {
        chai.request(server)
            .delete('/api/blind-level/delete-all/' + this.scheduleId)
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object').to.have.property('success')
                    done()
                } else {
                    done(err)
                }
            })
    })
})
