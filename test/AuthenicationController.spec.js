/* global it, describe, before */
// 'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
chai.should()
const chaiHttp = require('chai-http')
const server = require('../src/app')
const Player = require('../src/models/Player')

chai.use(chaiHttp)

describe('POST /Register', () => {
    before(async () => {
        // Reset user mode before each test
        await Player.query()
            .delete()
            .where({email: 'test@test.com'})
    })

    it('it should register a user', (done) => {
        let player = {
            email: 'test@test.com',
            password: '12345678',
            screenName: 'tobeornottobe'
        }

        chai.request(server)
            .post('/api/register')
            .send(player)
            .end((err, res) => {
                if (err) {
                    done(err)
                } else {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('email').to.eql('test@test.com')
                    res.body.should.have.property('screenName').to.eql('tobeornottobe')
                    done()
                }
            })
    })
})
