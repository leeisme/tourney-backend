/* global it, describe, beforeEach */
'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
chai.should()
const chaiHttp = require('chai-http')
const server = require('../src/app')
const moment = require('moment')
const TokenInserter = require('../test-utilty/TokenInserter')

chai.use(chaiHttp)

describe('CRUD TournamentSeries', () => {
    beforeEach(async () => {
        this.token = await TokenInserter.insertTokenForTestPlayer()
    })

    it('it should create a series', (done) => {
        const payload = {
            name: 'Test Tournament Series',
            startTime: moment().format('l'),
            endTime: moment().add(3, 'months')
        }

        chai.request(server)
            .post('/api/series')
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('id')
                    res.body.should.have.property('startTime')
                    res.body.should.have.property('endTime')
                    res.body.should.have.property('leagueId')
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should update a series', (done) => {
        const payload = {
            name: 'Updated Test Tournament Series',
            startTime: moment().format('l'),
            endTime: moment().add(3, 'months'),
            leagueId: 1
        }

        chai.request(server)
            .patch('/api/series/1')
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('id')
                    res.body.should.have.property('startTime')
                    res.body.should.have.property('endTime')
                    res.body.should.have.property('leagueId')
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should delete a series', (done) => {
        chai.request(server)
            .delete('/api/series/1')
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should return a paged list of series', (done) => {
        chai.request(server)
            .get('/api/series/0/1')
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    done()
                } else {
                    done(err)
                }
            })
    })
})
