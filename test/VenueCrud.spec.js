/* global it, describe, beforeEach */
'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
chai.should()
const chaiHttp = require('chai-http')
const server = require('../src/app')
const TokenInserter = require('../test-utilty/TokenInserter')
const TestSeeder = require('../test-utilty/TestSeeder')
const Venue = require('../src/models/Venue')

chai.use(chaiHttp)

describe('CRUD Venue', () => {
    beforeEach(async () => {
        this.token = await TokenInserter.insertTokenForTestPlayer()

        await Venue.query()
            .delete()
            .where('id', '>', 1)

        const venue = await TestSeeder.createVenue()
        this.venueId = venue.id
    })

    it('it should create a venue', (done) => {
        const payload = {
            name: 'Test Venue',
            active: true,
            description: 'A fun and relaxing venue',
            phone: '4104322243',
            email: 'test@test.com',
            web: 'https://www.somedomain.com'
        }

        chai.request(server)
            .post('/api/venue')
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('id')
                    res.body.should.have.property('name')
                    res.body.should.have.property('description')
                    res.body.should.have.property('phone')
                    res.body.should.have.property('email')
                    res.body.should.have.property('web')
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should update a venue', (done) => {
        const payload = {
            name: 'Test Venue Updated',
            active: true,
            description: 'A fun and relaxing venue',
            phone: '4104322243',
            email: 'test@test.com',
            web: 'https://www.somedomain.com'
        }

        chai.request(server)
            .patch('/api/venue/' + this.venueId)
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('success').to.equal(true)
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should delete a venue', (done) => {
        chai.request(server)
            .delete('/api/venue/1')
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    done()
                } else {
                    done(err)
                }
            })
    })
})
