/* global it, describe, beforeEach, before */
'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
chai.should()
const chaiHttp = require('chai-http')
const server = require('../src/app')
const moment = require('moment')
const TokenInserter = require('../test-utilty/TokenInserter')
const TestSeeder = require('../test-utilty/TestSeeder')

chai.use(chaiHttp)

describe('CRUD Tournament', () => {
    before(async () => {
        // Create Series
        const series = await TestSeeder.createSeries()
        const tournament = await TestSeeder.createTournament(series.id)
        this.tournamentId = tournament.id
        this.seriesId = series.id
    })

    beforeEach(async () => {
        this.token = await TokenInserter.insertTokenForTestPlayer()
    })

    it('it should create a tournament', (done) => {
        const payload = {
            name: 'Test Tournament',
            startTime: moment().add(3, 'days'),
            seriesId: this.seriesId,
            leagueId: 1
        }

        chai.request(server)
            .post('/api/tournament')
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('id')
                    res.body.should.have.property('name')
                    res.body.should.have.property('startTime')
                    res.body.should.have.property('seriesId')
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should update a tournament', (done) => {
        const payload = {
            name: 'Updated Test Tournament',
            startTime: moment().add(1, 'days'),
            seriesId: this.seriesId,
            leagueId: 1
        }

        chai.request(server)
            .patch('/api/tournament/' + this.tournamentId)
            .set('x-access-token', this.token)
            .send(payload)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.should.have.property('affected')
                    done()
                } else {
                    done(err)
                }
            })
    })

    it('it should delete a tournament', (done) => {
        chai.request(server)
            .delete('/api/tournament/' + this.tournamentId)
            .set('x-access-token', this.token)
            .end((err, res) => {
                if (!err) {
                    res.should.have.status(200)
                    res.body.should.have.property('success')
                    done()
                } else {
                    done(err)
                }
            })
    })
})
