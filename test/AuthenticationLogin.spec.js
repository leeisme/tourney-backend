/* global it, describe */
'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
chai.should()
const chaiHttp = require('chai-http')
const server = require('../src/app')

chai.use(chaiHttp)

describe('POST Player login', () => {
    const login = {
        email: 'testplayer@nodomain.com',
        password: '12345678'
    }

    it('it should login a user', (done) => {
        chai.request(server)
            .post('/api/login/player')
            .send(login)
            .end((err, res) => {
                if (err) {
                    done(err)
                } else {
                    res.should.have.status(200)
                    res.body.should.have.property('user')
                    done()
                }
            })
    })
})

describe('Password Resets', function () {
    it('it should request a password reset', (done) => {
        let data = {
            email: 'testplayer@nodomain.com'
        }

        chai.request(server)
            .post('/api/reset/player')
            .send(data)
            .end((err, res) => {
                if (err) {
                    done(err)
                } else {
                    res.should.have.status(200)
                    res.body.should.have.property('token')
                    res.body.should.have.property('expires')
                    done()
                }
            })
    })
})
