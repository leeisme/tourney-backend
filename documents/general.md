# Overview

Social Poker site for tracking and managing poker leagues and their games.

### Broad Functioniality

* System
  * Players
    * Registration
    * Authentication
    * Notifications
      * Restrictions/opt-outs
      * Types of notifications
      * Granularity of notifications
    * Social media integration
* League 
  * Player Relationships
    * Notifications
    * League membership
  * Series management
    * CRUD
    * Schedule
  * Tournament management
    * CRUD
    * Schedule
    * Results/Updating
    * Templates
    * Tournament Director Module
  * Blind Schedule
  * Points
    * Point Buckets
      * Entry points
      * Placement points
      * Series points
      * Tournament Points
      * Early check-in points
      * Frequent player points
    * Reset thresholds
    * Event triggering
  * Prizes
    * Prize triggers
    * Prize types
    * Prize actions
  * Series/Seasons Management
    * Parent of Tournaments
  * Promos
* Player 