// Update with your config settings.

module.exports = {

    development: {
        client: 'mysql',
        connection: {
            database: process.env.DB_NAME || 'tourney',
            user: process.env.DB_USER || 'root',
            password: process.env.DB_PASS || 'root',
            host: process.env.DB_HOST || 'tourney.test',
            port: process.env.DB_PORT || 3306
        },
        migrations: {
            directory: 'src/db/migrations'
        },
        seeds: {
            directory: 'src/db/seeds'
        }
    },

    testing: {
        client: 'mysql',
        connection: {
            database: process.env.DB_NAME || 'tourney_test',
            user: process.env.DB_USER || 'root',
            password: process.env.DB_PASS || 'root',
            host: process.env.DB_HOST || '172.20.0.2',
            port: process.env.DB_PORT || 3306
        },
        migrations: {
            directory: 'src/db/migrations'
        },
        seeds: {
            directory: 'src/db/seeds'
        }
    },

    staging: {
        client: 'postgresql',
        connection: {
            database: 'my_db',
            user: 'username',
            password: 'password'
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    },

    production: {
        client: 'postgresql',
        connection: {
            database: 'my_db',
            user: 'username',
            password: 'password'
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: 'src/migrations'
        }
    }

}
