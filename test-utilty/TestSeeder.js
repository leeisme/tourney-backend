const Series = require('../src/models/Series')
const League = require('../src/models/League')
const Player = require('../src/models/Player')
const Tournament = require('../src/models/Tournament')
const BlindSchedule = require('../src/models/BlindSchedule')
const BlindLevel = require('../src/models/BlindLevel')
const Venue = require('../src/models/Venue')

const JwtService = require('../src/services/JwtService')
const config = require('../src/config/config')

const moment = require('moment')

module.exports = {
    async createToken () {
        const player = await Player
            .query()
            .eager('league')
            .findOne({email: 'testplayer@nodomain.com'})

        const playerObj = {
            user: {
                id: player.id,
                email: player.email,
                screenName: player.screenName
            },
            iat: Math.floor(Date.now() / 1000) - 3000,
            leagueId: player.league.id
        }

        const ONE_WEEK = 60 * 60 * 24 * 7
        const token = await JwtService.signUser(playerObj, config.authentication.jwSecret, {
            expiresIn: ONE_WEEK
        })

        await Player.query()
            .patch({token: token})
            .where('email', '=', 'testplayer@nodomain.com')

        return token
    },
    async createSeries () {
        return Series.query().insertGraph({
            name: 'Mocked Series',
            startTime: moment().add(1, 'days').toDate(),
            endTime: moment().add(30, 'days').toDate(),
            leagueId: 1
        })
    },
    async createLeague () {
        return League.query().insertGraph({
            name: 'Test League',
            active: true,
            ownerId: 1
        })
    },
    async createTournament (seriesId) {
        return Tournament.query().insertGraph({
            name: 'Test Tournament',
            startTime: moment().add(3, 'days').toDate(),
            seriesId: seriesId,
            leagueId: 1
        })
    },
    async createBlindSchedule () {
        return BlindSchedule.query().insertGraph({
            name: 'Test Blind Schedule for deletion',
            active: true,
            leagueId: 1
        })
    },
    async createBlindLevel (levelNumber = 1, smallBlind = 10, bigBlind = 20) {
        return BlindLevel.query().insertGraph({
            scheduleId: 1,
            levelNumber: levelNumber,
            breakTime: 0,
            smallBlind: smallBlind,
            bigBlind: bigBlind,
            ante: 0,
            length: 20
        })
    },
    async createVenue () {
        return Venue.query().insertGraph({
            name: 'Test Venue from Seeder',
            active: true,
            description: 'Test venue from Seeder',
            phone: '4105555555',
            email: 'test@test.com',
            web: 'https://test.com',
            playerId: 1
        })
    }
}
