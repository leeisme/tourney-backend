const JwtService = require('../src/services/JwtService')
const config = require('../src/config/config')
const Player = require('../src/models/Player')

module.exports = {
    /**
     * Inserts a token for a test user
     * @return {Promise<void>}
     */
    async insertTokenForTestPlayer () {
        const player = await Player
            .query()
            .eager('league')
            .findOne({email: 'testplayer@nodomain.com'})

        const playerObj = {
            user: {
                id: player.id,
                email: player.email,
                screenName: player.screenName
            },
            iat: Math.floor(Date.now() / 1000) - 3000,
            leagueId: player.league.id,
            playerId: player.id
        }

        const ONE_WEEK = 60 * 60 * 24 * 7
        const token = await JwtService.signUser(playerObj, config.authentication.jwSecret, {
            expiresIn: ONE_WEEK
        })

        await Player.query()
            .patch({token: token})
            .where('email', '=', 'testplayer@nodomain.com')

        return token
    }
}
